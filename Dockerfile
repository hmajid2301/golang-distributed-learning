FROM golang:1.19-alpine AS builder

WORKDIR /temp

COPY go.mod go.sum ./
COPY internal/ ./internal/
COPY cmd/ ./cmd/

RUN go mod download && \
	go build -o ./app ./cmd/distributed-lock-service/main.go


FROM scratch AS production

EXPOSE 8080

WORKDIR /app

COPY --from=builder /temp/app ./

CMD ["./app"]