package main

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/go-redis/redis/v9"
)

func main() {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		exit := make(chan os.Signal, 1)
		signal.Notify(exit, os.Interrupt, syscall.SIGKILL)
		cancel()
	}()
	var wg sync.WaitGroup

	go mainLogic(ctx, client, &wg)
	time.Sleep(1 * time.Second)
	ctx = context.Background()
	go mainLogic(ctx, client, &wg)
	wg.Wait()
}

func mainLogic(ctx context.Context, client *redis.Client, wg *sync.WaitGroup) {
	wg.Add(1)
	defer wg.Done()
	workerNum := rand.Int()

	for {
		select {
		case <-ctx.Done():
			fmt.Println("Break the loop")
			break
		default:
			fmt.Printf("Obtaining %v\n", workerNum)
			obtain(client, workerNum)
			fmt.Printf("Doing work %v\n", workerNum)
			doWork(workerNum, client)
		}
	}
}

func obtain(client *redis.Client, workerNum int) {
	ctx := context.Background()

	for {
		fmt.Printf("Attempting to acquire lock %v\n", workerNum)
		lock := client.Get(ctx, "LOCK")

		var ok bool
		if lock.Val() == "" {
			ok = set(client, ctx, workerNum)
		} else {
			ok = isObtainable(lock, client, ctx, workerNum)
		}

		if ok {
			break
		}
		fmt.Println("Failed to acquire lock waiting 2 second")
		time.Sleep(2 * time.Second)
	}
}

type Lock struct {
	LockName string
	TTL      time.Time
}

func (l Lock) MarshalBinary() ([]byte, error) {
	return json.Marshal(l)
}

func isObtainable(lock *redis.StringCmd, client *redis.Client, ctx context.Context, workerNum int) bool {
	data := Lock{}
	err := json.Unmarshal([]byte(lock.Val()), &data)
	if err != nil {
		fmt.Println("Failed to unmarshal JSON")
	}
	if lock.Err() != nil || time.Now().Local().After(data.TTL) {
		ok := set(client, ctx, workerNum)
		if ok {
			return true
		}
	}
	return false
}

func set(client *redis.Client, ctx context.Context, workerNum int) bool {
	payload := Lock{LockName: strconv.Itoa(workerNum), TTL: time.Now().Local().Add(time.Second * 5)}
	lock := client.Set(ctx, "LOCK", payload, 0)
	if lock.Err() != nil {
		fmt.Println("Failed to acquire lock")
		return false
	}
	return true
}

func doWork(workerNum int, client *redis.Client) {
	ctx := context.Background()
	fmt.Println("Acquired lock")
	for {
		fmt.Printf("Doing some work %v\n", workerNum)
		time.Sleep(4 * time.Second)

		fmt.Println("Reacquiring lock")
		ok := set(client, ctx, workerNum)
		if !ok {
			fmt.Println("Failed to reacquire lock")
			break
		}
	}
}
