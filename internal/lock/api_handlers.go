package lock

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

type LockAPIHandler struct {
	Service LockService
}

func NewHandler(service LockService) *LockAPIHandler {
	return &LockAPIHandler{
		service,
	}
}

func (lc *LockAPIHandler) AddLockHandler(c *gin.Context) {
	newLock := NewLock{}
	err := c.ShouldBindBodyWith(&newLock, binding.JSON)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request, check the payload is valid."})
	}
	addedLock, err := lc.Service.Add(newLock)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Unable to create new lock"})
	}
	c.JSON(http.StatusCreated, AddedLockOut{
		Name:         addedLock.Name,
		ServiceName:  addedLock.ServiceName,
		ResourceName: addedLock.ResourceName,
		Expiry:       addedLock.Expiry,
	})
}
