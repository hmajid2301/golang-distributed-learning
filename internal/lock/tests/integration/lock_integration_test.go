package integration_test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hmajid2301/distributed-lock-service/internal/api"
	"gitlab.com/hmajid2301/distributed-lock-service/internal/lock"
)

func TestAddLockHandler(t *testing.T) {
	repo, _ := lock.NewRepository("mongodb://username:password@localhost:27017", "test", "lock", 5)

	service := lock.NewService(repo)
	handler := lock.NewHandler(service)
	router := gin.Default()
	api.SetupRouter(handler, router)
	expiry := time.Now().Unix() + 600
	newLock := lock.NewLock{
		ServiceName:  "TestService",
		ResourceName: "WorkItem1",
		Expiry:       expiry,
	}
	payload, _ := json.Marshal(newLock)
	req, _ := http.NewRequest("POST", "/lock", bytes.NewBuffer(payload))
	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	expectedResp := &lock.AddedLockOut{Name: "bb41b1c4-bb1a-4015-b767-bbe5e78617d8", ServiceName: "TestService", ResourceName: "WorkItem1", Expiry: expiry}
	actualResp := &lock.AddedLockOut{}
	json.Unmarshal(responseData, actualResp)
	assert.Equal(t, expectedResp.Expiry, actualResp.Expiry)
	assert.Equal(t, expectedResp.ServiceName, actualResp.ServiceName)
	assert.Equal(t, expectedResp.ResourceName, actualResp.ResourceName)
	assert.NotNil(t, expectedResp.Name)
	assert.Equal(t, http.StatusCreated, w.Code)
}
