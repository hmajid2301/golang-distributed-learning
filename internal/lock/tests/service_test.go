package service_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/hmajid2301/distributed-lock-service/internal/lock"
	lockMock "gitlab.com/hmajid2301/distributed-lock-service/internal/lock/mocks"
)

var lockRepo lockMock.LockRepository
var lockModel lock.Lock

func TestAddLock(t *testing.T) {
	exampleLock := &lock.Lock{
		Name:         "Test",
		ServiceName:  "TestService",
		ResourceName: "WorkItem1",
		Expiry:       time.Now().Unix() + 600,
	}

	t.Run("Should add new lock", func(t *testing.T) {
		lockRepo.On("Add", mock.AnythingOfType("lock.NewLock")).Return(exampleLock, nil).Once()

		newLock := lock.NewLock{
			ServiceName:  "TestService",
			ResourceName: "WorkItem1",
			Expiry:       time.Now().Unix() + 600,
		}

		lockService := &lock.Service{
			Repository: &lockRepo,
		}
		addedLock, err := lockService.Add(newLock)

		assert.Equal(t, newLock.ResourceName, addedLock.ResourceName)
		assert.Equal(t, newLock.ServiceName, addedLock.ServiceName)
		assert.Equal(t, newLock.Expiry, addedLock.Expiry)
		assert.NotNil(t, addedLock.Name, "Test")
		assert.Nil(t, err)
	})
	t.Run("Should not add new lock, expired in the past", func(t *testing.T) {
		lockRepo.On("Add", mock.AnythingOfType("lock.NewLock")).Return(nil, nil).Once()

		newLock := lock.NewLock{
			ServiceName:  "TestService",
			ResourceName: "WorkItem1",
			Expiry:       time.Now().Unix() - 600,
		}

		lockService := &lock.Service{
			Repository: &lockRepo,
		}
		_, err := lockService.Add(newLock)
		assert.Equal(t, err, lock.LockExpiredErr)
	})
}
