package lock

import "time"

type LockService interface {
	Add(lock NewLock) (*Lock, error)
}

type Service struct {
	Repository LockRepository
}

func NewService(repo LockRepository) LockService {
	return &Service{
		Repository: repo,
	}
}

func (s *Service) Add(lock NewLock) (*Lock, error) {
	now := time.Now().Unix()

	if now > lock.Expiry {
		return nil, LockExpiredErr
	}
	return s.Repository.Add(lock)
}
