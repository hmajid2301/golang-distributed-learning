package lock

type LockRepository interface {
	Add(lock NewLock) (*Lock, error)
}

