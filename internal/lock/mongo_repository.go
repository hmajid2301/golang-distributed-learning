package lock

import (
	"context"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type LockMongoRepository struct {
	collection *mongo.Collection
	timeout    time.Duration
}

// TODO: make generic not specific to lock domain
func NewRepository(uri, dbName, collectionName string, mongoTimeout int) (LockRepository, error) {
	repo := &LockMongoRepository{
		timeout: time.Duration(mongoTimeout) * time.Second,
	}
	client, err := newClient(uri, mongoTimeout)
	if err != nil {
		return nil, err
	}
	repo.collection = client.Database(dbName).Collection(collectionName)
	return repo, nil
}

func newClient(mongoURL string, mongoTimeout int) (*mongo.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(mongoTimeout)*time.Second)
	defer cancel()
	client, e := mongo.Connect(ctx, options.Client().ApplyURI(mongoURL))
	if e != nil {
		return nil, e
	}
	if e = client.Ping(ctx, readpref.Primary()); e != nil {
		return nil, e
	}
	return client, e
}

func (l *LockMongoRepository) Add(newLock NewLock) (*Lock, error) {
	ctx, cancel := context.WithTimeout(context.Background(), l.timeout*time.Second)
	defer cancel()

	lock := &Lock{
		Name:         uuid.New().String(),
		ServiceName:  newLock.ServiceName,
		ResourceName: newLock.ResourceName,
		Expiry:       newLock.Expiry,
	}

	_, err := l.collection.InsertOne(ctx, lock)
	if err != nil {
		return nil, err
	}

	return lock, nil
}
