package lock

import "errors"

var (
	LockExpiredErr = errors.New("Lock has already expired")
)
