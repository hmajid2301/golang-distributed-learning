package lock

type NewLock struct {
	ServiceName  string `validate:"required"`
	ResourceName string `validate:"required"`
	Expiry       int64  `validate:"required"`
}

type Lock struct {
	Name         string
	ServiceName  string
	ResourceName string
	Expiry       int64
}
