package lock

type AddNewLock struct {
	ServiceName  string `json:"service_name" validate:"required"`
	ResourceName string `json:"resource_name" validate:"required"`
	Expiry       int64  `json:"expiry" validate:"required"`
}

type AddedLockOut struct {
	Name         string `json:"name"`
	ServiceName  string `json:"service_name"`
	ResourceName string `json:"resource_name"`
	Expiry       int64  `json:"expiry"`
}
