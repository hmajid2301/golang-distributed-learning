package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/hmajid2301/distributed-lock-service/internal/lock"
)

func SetupRouter(lockController *lock.LockAPIHandler, g *gin.Engine) {
	lockGrp := g.Group("/lock")
	lockGrp.POST("", lockController.AddLockHandler)
}
