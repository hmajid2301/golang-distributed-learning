# Learning: Distributed Systems with Golang

This is a repo for me to learn some concepts around distributed computing whilst also
practising Golang. 

> Note: This code is not production ready code; lots of hard-coded values no constants etc.

Appendix:

- <a href="https://www.flaticon.com/free-icons/lock" title="lock icons">Lock icons created by Freepik - Flaticon</a>
