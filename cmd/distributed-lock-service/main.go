package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/hmajid2301/distributed-lock-service/internal/api"
	"gitlab.com/hmajid2301/distributed-lock-service/internal/lock"
)

func main() {
	retCode := MainLogic()
	os.Exit(retCode)
}

// TODO: add logging
func MainLogic() int {
	// TODO: make this config not hard coded
	repo, err := lock.NewRepository("mongodb://username:password@localhost:27017", "lock_service", "lock", 5)
	if err != nil {
		return 1
	}

	service := lock.NewService(repo)
	handler := lock.NewHandler(service)
	router := gin.Default()
	api.SetupRouter(handler, router)
	router.Run()
	return 0
}
